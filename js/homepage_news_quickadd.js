/**
 * @file
 * Adds a button to insert the title and link into relevant fields.
 */

(function ($) {

  $(document).ready(function () {

    // Add a button to insert the title and link into the relevant fields for each entry
    // in the latest news releases table.
    $('.news-item', '#news-releases').each(function (index, element) {
      var $item = $(this);
      $('<td><a href="#">Insert</a></td>')
        .insertAfter($('.news-item-link', $item))
        .click(function (event) {
          event.preventDefault();
          $('#edit-title').val($('.news-item-title', $item).text()); // Insert the title
          $('#edit-field-link-url-und-0-url').val($('.news-item-link', $item).text()); // insert the url.
        });
    });

    // Add a table column header (to not break table formatting)
    $('<th>Insert</th>').appendTo($('thead tr', '#news-releases'));

  });

})(jQuery);
